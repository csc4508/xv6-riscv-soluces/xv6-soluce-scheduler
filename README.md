# Xv6 for RISC-V -- Télécom SudParis classes

This project is a fork of the [amazing project xv6](https://github.com/mit-pdos/xv6-riscv) developed at MIT for the CSC4508 course given at TSP.

You can find the original README under the name [README](./README).

We explain in the remaining sections how we can synchronize this repository with the
repositories that contain the solutions, and with the repository of the MIT.

## Repository setup

Clone the project, create the remotes and associate local branches to remote ones.

```bash
# Clone the repository.
git clone ssh://git@gitlab.inf.telecom-sudparis.eu:2222/csc4508/xv6-riscv.git
cd xv6-riscv

# Add a remote for the upstream repository.
git remote add mit https://github.com/mit-pdos/xv6-riscv.git

# Add remotes repositories for the solutions of the labs.
git remote add scheduler-soluce ssh://git@gitlab.inf.telecom-sudparis.eu:2222/csc4508/xv6-riscv-soluces/xv6-soluce-scheduler.git
git remote add futex-soluce ssh://git@gitlab.inf.telecom-sudparis.eu:2222/csc4508/xv6-riscv-soluces/xv6-soluce-futex.git
git remote add shm-soluce ssh://git@gitlab.inf.telecom-sudparis.eu:2222/csc4508/xv6-riscv-soluces/xv6-soluce-shm.git
git remote add mmap-soluce ssh://git@gitlab.inf.telecom-sudparis.eu:2222/csc4508/xv6-riscv-soluces/xv6-soluce-mmap.git

# Fetch all the repositories.
git fetch --all

# Create the local branches to track the remote branches.
git checkout -b scheduler-lab -t origin/scheduler-lab

git checkout -b scheduler-soluce -t scheduler-soluce/scheduler-lab
git checkout -b futex-soluce -t futex-soluce/master
git checkout -b shm-soluce -t shm-soluce/master
git checkout -b mmap-soluce-exo1 -t mmap-soluce/exo1
git checkout -b mmap-soluce-exo2 -t mmap-soluce/exo2
git checkout -b mmap-soluce-exo3 -t mmap-soluce/exo3
git checkout -b mmap-soluce-exo4 -t mmap-soluce/exo4

# Make pull and push symmetric.
git config push.default upstream

# Switch to master.
git checkout master
```

In order to check that everything is OK, just run `git branch -vv`: you will see which local branch is associated to which remote branch.

## Updating from upstream

To include updates from MIT's upstream:

```bash
# Fetch updates from upstream
git fetch mit

# Ideally, merge upstream changes into master.
# We to keep a clean history, be prefer rebasing (see next section).
git rebase mit/riscv

# We rewrote the history, so force push.
git push --force
```

## Rebasing

Rebasing an history pushed on a remote repository is not a good idea because it could break what other developers are doing.
However, in our case, since we are only few teachers, we can, sometimes, rebase our histories (even in the remotes).

As a courtesy, we should warn our colleagues that we forcefully changed the history upstream.

Let's take an example: I made some modifications in master and I want to rebase the other branches (instead of merging in order to avoid an ugly history):

```bash
git checkout scheduler-lab
git rebase master
git push --force                    # because we modify the history of a remote

git checkout scheduler-soluce
git rebase scheduler-lab
git push --force                    # because we modify the history of a remote

git checkout futex-soluce
git rebase master
git push --force                    # because we modify the history of a remote

# etc.... for all the branches
```

