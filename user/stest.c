#include "kernel/types.h"
#include "user/user.h"

#pragma GCC push_options
#pragma GCC optimize ("O0")

void
work() {
  for (int i = 0; i < 3; i++)
    for (int j = 0; j < (2 << 28); j++);
}

int
main(int argc, char *argv[])
{
  int wpid;

  printf("INF\tprocess %d (parent) starts\n", getpid());

  for (int i = 0; i < 3; i++) {
    int fork_ret;

    if ((fork_ret = fork()) < 0) {
      printf("ERR\tfailed forking child #%d\n", i+1);
      exit(1);
    } else if (fork_ret == 0) {
      printf("INF\tprocess %d (child #%d) starts\n", getpid(), i+1);
      work();
      printf("INF\tprocess %d (child #%d) ends\n", getpid(), i+1);
      exit(0);
    } else {
      printf("INF\tforked child #%d\n", i+1);
    }
  }

  work();

  while ((wpid = wait(0)) >= 0) {
    printf("INF\twaited child with PID %d\n", wpid);
  }

  printf("INF\twaited all children\n");

  printf("INF\tprocess %d (parent) ends\n", getpid());

  exit(0);
}

#pragma GCC pop_options
